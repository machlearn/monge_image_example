# Description

This code was used to generate the digit image example in the following paper:

Zac Cranko, Aditya Krishna Menon, Richard Nock, Cheng Soon Ong, Zhan Shi, Christian Walder
**Monge blunts Bayes: Hardness Results for Adversarial Training**, ICML 2019.

[https://arxiv.org/abs/1806.02977](https://arxiv.org/abs/1806.02977)

# Usage

## monge_image_example.py

This main program generates adversarially perturbed digit images from the USPS dataset. A key parameter is the adversary strength. To reproduce the results from the paper you would run

```
python monge_image_example.py --l1_radius_increment_ratio "0.15"
python monge_image_example.py --l1_radius_increment_ratio "0.30"
python monge_image_example.py --l1_radius_increment_ratio "0.45"
python monge_image_example.py --l1_radius_increment_ratio "0.60"
```

## monge_image_example_logres.py

This program needs to be run after *monge_image_example.py*. It will train and test logistic regression on the various combinations of clean and adversarially perturbed datasets, as described in the paper. You should point it to the directory containing the output from (and **only** the output from) *monge_image_example.py*. For example,

```
python monge_image_example_logres.py --basepath "'/some/directory/'"
```

## help

To see a list of options, run either program with a --help flag, for example, from the command line you can run

```
python monge_image_example.py --help
```
