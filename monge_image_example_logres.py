import os, glob
import numpy as np
from sklearn.datasets import make_moons, load_digits
import matplotlib.pyplot as plt
from scipy.optimize import minimize, NonlinearConstraint
from sklearn.linear_model import LogisticRegression, LogisticRegressionCV
from sklearn.metrics import log_loss

try:
    import gridtools.helpers as gh
    import gridtools.paths as gp
except:
    raise Exception('install gridtools from https://bitbucket.org/cwalder/gridtools')


import matplotlib as mpl

latex_preamble = [
    r'\usepackage{amsmath,bm}',
    r'\newcommand\what{\hat{\bm{w}}}',
    r'\newcommand\tr{^\top}',
    r'\newcommand\dt[1]{\left|#1\right|}',
]
mpl.rcParams['text.usetex'] = True
mpl.rcParams['text.latex.preamble'] = latex_preamble
os.environ['PATH'] = '%s:/Library/TeX/texbin/' % os.environ['PATH']

import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

########################################################################

default_path = os.path.expanduser('~/output/monge_image_example/test_1')
rows_per_page = 5

all_tex = '''
\\documentclass{article}
\\usepackage{graphicx}
\\let\\mypdfximage\\pdfximage 
\\protected\\def\\pdfximage{\\immediate\\mypdfximage} 
\\begin{document}
\\centering
\\newcommand{\\thescale}{0.19}
%s
\end{document}
'''

########################################################################

def evaluate(xc, xa, y, splits, inner_n_cv):
    pccs, pcas, pacs, paas = [], [], [], []
    for test in splits:
        train = np.array(list(set(range(len(y))).difference(set(test))))
        mc = LogisticRegressionCV(cv=inner_n_cv).fit(xc[:, train].T, y[train])
        ma = LogisticRegressionCV(cv=inner_n_cv).fit(xa[:, train].T, y[train])
        pccs.append(log_loss(y_pred=mc.predict_proba(xc[:, test].T), labels=[0, 1], y_true=y[test]))
        pcas.append(log_loss(y_pred=mc.predict_proba(xa[:, test].T), labels=[0, 1], y_true=y[test]))
        pacs.append(log_loss(y_pred=ma.predict_proba(xc[:, test].T), labels=[0, 1], y_true=y[test]))
        paas.append(log_loss(y_pred=ma.predict_proba(xa[:, test].T), labels=[0, 1], y_true=y[test]))
    return tuple(map(np.mean, (pccs, pcas, pacs, paas)))


def raw_main(
    basepath=default_path,
    inner_n_cv=10,
):

    path = gp.output('logres/', basepath=basepath)
    gh.dumpArgsToDirectory(path)
    tee = gh.Tee('%s/output.txt' % path, compress=False)

    out_path = gp.output('out', basepath=path, datestamp=False)

    roots = glob.glob('%s/*/*/final/' % basepath)

    def load(root):
        args = gh.load('%s/../../dumpArgs/argdict.pickle' % root)
        data = gh.load('%s/data.pickle' % root)
        data['args'] = args
        return data

    results = sorted([load(root) for root in roots], key=lambda d: d['args']['l1_radius_increment_ratio'])

    n = results[0]['args']['n']

    splits = [[i] for i in range(2*n)]

    for iresult in range(len(results)):

        xc = np.hstack((results[iresult]['xp'], results[iresult]['xn']))
        xa = np.hstack((results[iresult]['xpa'], results[iresult]['xna']))
        y = np.concatenate((np.zeros(n), np.ones(n)))
        r = evaluate(xc, xa, y, splits, inner_n_cv)
        print(' & '.join('%.2f' % _ for _ in r) + ' \\')

    print('cd %s' % out_path)

    tee.to_file()

main = gh.auto_parser_inspector()(gh.dumpArgs()(raw_main))


if __name__ == '__main__':

    main()
