import os
import numpy as np
from sklearn.datasets import make_moons, load_digits
import matplotlib.pyplot as plt
import ot
from scipy.optimize import minimize, NonlinearConstraint

try:
    import gridtools.helpers as gh
    import gridtools.paths as gp
except:
    raise Exception('install gridtools from https://bitbucket.org/cwalder/gridtools')


import matplotlib as mpl

latex_preamble = [
    r'\usepackage{amsmath,bm}',
    r'\newcommand\what{\hat{\bm{w}}}',
    r'\newcommand\tr{^\top}',
    r'\newcommand\dt[1]{\left|#1\right|}',
]
mpl.rcParams['text.usetex'] = True
mpl.rcParams['text.latex.preamble'] = latex_preamble
os.environ['PATH'] = '%s:/Library/TeX/texbin/' % os.environ['PATH']

import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

########################################################################

metric = 'sqeuclidean'
default_dataset_options = {
    'moons': {'noise': 0.0, 'maxiter': 1000},
    'digits': {'yp': 1, 'yn': 3, 'maxiter': 30}
}
best_cost = np.inf
do_plot = True
default_dataset_name = 'digits'
default_steps = 1
default_l1_radius_increment_ratio = 0.8
default_n = 100
random_state = 0
dataset_options_overrides = {}
basepath = None

########################################################################

def objective(rp, rn, xp, xn):

    global best_cost
    global do_plot

    assert xp.shape == xn.shape == rp.shape == rn.shape

    xp = xp + rp
    xn = xn + rn
    dist = ot.dist(xp.T, xn.T, metric=metric)
    mu = ot.emd([], [], dist)
    cost = (mu * dist).flatten().sum()

    if cost < best_cost:
        print('best_cost %.3f' % cost)
        best_cost = cost
        do_plot = True

    return cost


def matify(rlinear, d, n):
    return rlinear.reshape(d, 2*n)


def deserialise(r, d, n):
    rmat = matify(r, d, n)
    rp = rmat[:, :n]
    rn = rmat[:, n:]
    return rp, rn


def optimise_perturbations(xp, xn, l1_radius, callback, maxiter):

    assert xp.shape == xn.shape, (xp.shape, xn.shape)
    (d, n) = xp.shape

    def obj(r, xp, xn):
        rp, rn = deserialise(r, d, n)
        return objective(rp, rn, xp, xn)

    m = xp.size
    r0 = np.zeros(2*m, dtype=np.float)

    l1_norm = lambda _: np.abs(_).flatten().sum()
    l1_ineq = lambda _: l1_radius - l1_norm(_)
    constraints = []
    for i in range(2*n):
        coni = lambda r, i=i: l1_ineq(matify(r, d, n)[:, i])
        constraints.append({'type': 'ineq', 'fun': coni})

    res = minimize(fun=obj, x0=r0, args=(xp, xn), options={'maxiter': maxiter, 'disp': True}, constraints=constraints, callback=callback)

    return res


def plot_2d(xp, xn, xpa, xna, path, frame_index, axis, is_callback):

    global do_plot

    if is_callback and not do_plot:
        return None
    else:
        do_plot = False

    sub_path = gp.output('frame_%.5i/%s' % (frame_index, 'final' if not is_callback else 'intermediate'), basepath=path, datestamp=is_callback, create=True)
    print(sub_path)

    dist = ot.dist(xp.T, xn.T, metric=metric)
    mu = ot.emd([], [], dist)

    pdffn = '%s/plot.pdf' % sub_path
    pdf = PdfPages(pdffn)

    plt.figure()

    plt.plot(xp[0, :], xp[1, :], 'b.', label=r'$x^{(p)}$')
    plt.plot(xn[0, :], xn[1, :], 'r.', label=r'$x^{(n)}$')
    plt.plot(xpa[0, :], xpa[1, :], 'b+', label=r'$a(x^{(p)})$')
    plt.plot(xna[0, :], xna[1, :], 'rx', label=r'$a(x^{(p)})$')
    ot.plot.plot2D_samples_mat(xp.T, xn.T, mu, c=[.5, .5, 1])
    plt.legend(loc='best')
    axis = plt.axis(axis)

    pdf.savefig()
    pdf.close()

    plt.close('all')

    return sub_path


def plot_images(xp, xn, xpa, xna, path, frame_index, axis, is_callback):

    global do_plot

    if is_callback and not do_plot:
        return None
    else:
        do_plot = False

    sub_path = gp.output('frame_%.5i/%s' % (frame_index, 'final' if not is_callback else 'intermediate'), basepath=path, datestamp=is_callback, create=True)
    print(sub_path)

    for label, x, xa in [('p', xp, xpa), ('n', xn, xna)]:

        for i in range(x.shape[1]):

            xi = x[:, i].reshape((8, 8))
            xai = xa[:, i].reshape((8, 8))
            diffi = xi - xai

            pdffn = os.path.expanduser('%s/%s_%.5i.pdf' % (sub_path, label, i))
            pdf = PdfPages(pdffn)

            for xplot in (xi, xai, diffi):
                plt.figure()
                plt.imshow(xplot, cmap='gray')
                plt.colorbar()
                pdf.savefig()

            pdf.close()

            plt.close('all')

    return sub_path


def run_one(xp, xn, l1_radius, path, frame_index, axis, maxiter):

    d, n = xp.shape

    def process_x(x, is_callback):

        rp, rn = deserialise(x, d, n)
        xpa, xna = xp + rp, xn + rn
        if xp.shape[0] == 2:
            sub_path = plot_2d(xp, xn, xpa, xna, path, frame_index, axis, is_callback)
        else:
            sub_path = plot_images(xp, xn, xpa, xna, path, frame_index, axis, is_callback)

        gh.dump(dict(xp=xp, xn=xn, rp=rp, rn=rn, xpa=xpa, xna=xna), filename='%s/data.pickle' % sub_path)

        if is_callback:
            return False
        else:
            return xpa, xna

    callback = lambda x: process_x(x, is_callback=True)

    res = optimise_perturbations(xp=xp, xn=xn, l1_radius=l1_radius, callback=callback, maxiter=maxiter)

    return process_x(res.x, is_callback=False)


def raw_main(
        dataset_name=default_dataset_name,
        replace_clean=True,
        steps=default_steps,
        l1_radius_increment_ratio=default_l1_radius_increment_ratio,
        n=default_n,
        random_state=0,
        dataset_options_overrides={},
        basepath=None,
):

    path = gp.output('monge_image_example/', basepath=basepath)
    gh.dumpArgsToDirectory(path)
    tee = gh.Tee('%s/output.txt' % path, compress=False)

    dataset_options = default_dataset_options[dataset_name]
    dataset_options.update(dataset_options_overrides)
    print('dataset_options:\n', dataset_options)

    if dataset_name == 'moons':

        noise = dataset_options['noise']
        X, y = make_moons(n * 2, shuffle=False, noise=noise, random_state=random_state)
        xp = X[:n, :].T
        xn = X[n:, :].T
        assert (y[:n] == 0).all() and (y[n:] == 1).all()
        axis = (-1.3, 2.2, -1, 1.5)

    elif dataset_name == 'digits':

        X, y = load_digits(n_class=10, return_X_y=True)
        assert set(y) == set(range(10))
        xp = X[y == dataset_options['yp'], :].T
        xn = X[y == dataset_options['yn'], :].T
        np.random.seed(random_state)
        isp = np.random.permutation(xp.shape[1])
        isn = np.random.permutation(xn.shape[1])
        xp = xp[:, isp]
        xn = xn[:, isn]
        xp = xp[:, :n]
        xn = xn[:, :n]
        axis = None

    else:

        raise Exception('dataset_name ''%s''' % dataset_name)

    assert xp.shape[1] == xn.shape[1] == n

    cp, cn = [_.mean(axis=1) for _ in (xp, xn)]
    c_l1_dist =  np.abs(cp-cn).sum()
    l1_increment = l1_radius_increment_ratio * c_l1_dist

    if replace_clean:
        l1_radius_list = np.ones(steps) * l1_increment
    else:
        l1_radius_list = np.arange(0, steps) * l1_increment

    for i, l1_radius in enumerate(l1_radius_list):
        xpa, xna = run_one(xp=xp, xn=xn, l1_radius=l1_radius, path=path, frame_index=i, axis=axis, maxiter=dataset_options['maxiter'])
        if replace_clean:
            xp, xn = xpa, xna

    tee.to_file()

main = gh.auto_parser_inspector()(gh.dumpArgs()(raw_main))


if __name__ == '__main__':

    main()
